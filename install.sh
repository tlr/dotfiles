#!/bin/bash
set -o errexit
set -o pipefail
set -o nounset


#
# Installs DOTFILES into home directory
#
#  copy/<files> are copied into homedir
#  link/<files> are symlinks from home directory to locaiton in ~/dotfiles/link/<file>
#

# 
# The EXPORT enables this to work on Windows... it will be safely ignored on Linux 
#
# Requirements for Windows:
#   1. You must have cygwin
#   2. You MUST exectue this script as Administrator
#

export CYGWIN="winsymlinks:native"


########## Variables

dir=~/dotfiles                  
backup_dir=~/dotfiles_saved

link_dir=$dir/link
copy_dir=$dir/copy

git_templatedir=~/.gitfiles/templates
vimundo_dir=~/.vimundo



########## Functions 

create_backup_dir()
{
   echo -n "==> Creating $backup_dir for backup of any existing dotfiles in ~ ... " >&2 
   mkdir -p $backup_dir >/dev/null 2>&1
   echo "done." >&2
}


create_symlinks() 
{
   #
   # Create the symlinks to ~/dotfiles/link 
   #
   # move any existing dotfiles in homedir to dotfiles_old directory, 
   # then create symlinks from the homedir to any files in the ~/dotfiles 
   # directory specified in $files

   echo "==> Creating symlinks from $link_dir ... " >&2
   for file in `ls $link_dir` ; do 
       [[ -e ~/.$file ]] && [[ ! -L ~/.$file ]] &&  mv ~/.$file $backup_dir/ > /dev/null 2>&1
       echo "    => Creating symlink: .$file --> $link_dir/$file " >&2
       ln -sf $link_dir/$file ~/.$file
       rc=$?
       if [[ $rc -ne 0 ]] ; then
          echo "create_symlinks() failed" >&2
          exit $rc 
       fi
   done
}

copy_dotfiles()
{
   echo "==> Copying files from $copy_dir ..." >&2
   for file in `ls $copy_dir` ; do 
      [[ -e ~/.$file  ]] && mv ~/.$file $backup_dir > /dev/null 2>&1
      echo "    => Copying file: $copy_dir/$file " >&2
      cp -f $copy_dir/$file ~/.$file >/dev/null 2>&1
      rc=$?
      if [[ $rc -ne 0 ]] ; then
         echo "copy_dotfiles() failed" >&2
         exit $rc 
      fi 
   done
   echo "done." >&2
}

create_special_dirs()
{
   echo "==> Creating special dirs ... " >&2
   if [[ ! -d $vimundo_dir ]] ; then
      echo -n "    => Making dir: $vimundo_dir" >&2
      mkdir -p $vimundo_dir >/dev/null 2>&1
      rc=$?
      if [[ $rc -ne 0 ]] ; then
         echo "create_special_dirs() failed" >&2
         exit $rc 
      fi 
      echo "done." >&2
   fi
}

update_git_config()
{
    if hash git && [[ -d "$git_templatedir" ]] ; then
       >&2 echo -n "==> Configuring git global templatedir = $git_templatedir... " >&2
       git config --global init.templatedir '$git_templatedir'
       if [[ $rc -ne 0 ]] ; then
         echo "update_git_config() failed" >&2
         exit $rc 
      fi 
      echo "done." >&2
    fi
}


############################
## MAIN
############################

main()
{
   # change to the dotfiles directory
   echo -n "==> Changing directory to: $dir" >&2
   cd $dir

   create_backup_dir

   create_symlinks 
   copy_dotfiles
   create_special_dirs
   update_git_config 
}

main "$@"
