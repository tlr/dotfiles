[user]
	name = Terry Rorabaugh
	email = tlr@mitre.org

[core]
    editor = vim
    eol = lf
    autocrlf = input
    whitespace = blank-at-eol,-blank-at-eof,-space-before-tab 
	trustctime = false

[init]
    templatedir = ~/.gitfiles/templates/

[commit]
    template = ~/.gitfiles/COMMIT_MSG

[color]
    ui = true

[color "branch"]
    current = yellow reverse
    local = yellow
    remote = green

[color "diff"]
    meta = yellow bold
    frag = magenta bold
    old = red bold
    new = green bold

[color "status"]
    added = yellow
    changed = green
    untracked = red

[web]
    browser = google-chrome

[push]
	default = matching

[alias] 

  # step back one commit and leave the changes from that commit staged
  undo = reset --soft HEAD^

  # ammend the staged changes to your last commit
  amend = commit -a --amend -C HEAD


  # Adding
  a     = add
  ae    = add --edit
  ai    = add --interactive
  ap    = add --patch
  au    = add --update

  # Branching
  b     = branch
  bls   = branch -v # list branches with last commit in
 
  brm   = branch -d # remove if branch-to-be-removed if fully merged into HEAD
  brmf  = branch -D # force removal

  # bt - branch track configures current branch to "track" same name from origin or specified remote
  bt    = "!f() { B=$(git symbolic-ref HEAD|cut -f3 -d/) && git config branch.$B.remote ${1:-origin} && git config branch.$B.merge refs/heads/$B; }; f"

  # rename branch
  bmv   = branch -m 

  #
  # Clean up a branch that has been merged 
  #   1) Switch to master
  #   2) bring master up to origin
  #   3) deletes all branches already merged into master
  #
  bdone = "!f() { git checkout ${1-master} && git up && git bclean ${1-master}; }; f"
  bclean = "!f() { git branch --merged ${1-master} | grep -v " ${1-master}$" | xargs -r git branch -d; }; f"
   
  # Config
  cg    = config --global
  clg   = config --global --list
  c     = config
  cl    = config --list
  ce    = config --edit
  ceg   = config --global --edit 

  # Checkout
  co    = checkout

  # previous one
  co-   = checkout -

  # into new branch
  cob   = checkout -b       
 
  # master branch
  com   = "!f() { git fetch; git checkout -b master origin/master; }; f"   
  cot   = "!f() { git fetch; git checkout -b ${1##*/} $1; }; f "           

  # new branch off master
  con   = "!f() { git fetch; git checkout -b $1 origin/master }; f "      

  # Commit
  cm = !git add -A && git commit -m

  # commit all using previous commit message as a template
  cia   = commit -a -e -C HEAD -v --reset-author --untracked-files=no

  # commit staged using prev commit message as a template
  ci    = commit    -e -C HEAD -v --reset-author --untracked-files=no

  # commit using branch name as an initial msg
  ciab  = !B=$(git symbolic-ref HEAD 2>/dev/null) && git commit -v -a --edit -m "${B##refs/heads/}:" --untracked-files=no

  cii   = commit --interactive

  # edit last commts message
  cid   = commit --amend -v   
  cim   = commit -m
  cip   = commit -am 'WIP!'

  # change author of latest commit
  # assumes properly configured author to be now set in the local config
  ciu   ="!git commit --amend --author=\"`git config --local --get user.name` <`git config --local --get user.email`>\""

  chp   = cherry-pick


  #--- diff
  d     = diff
  ds    = diff --staged
  du    = diff @{upstream}
  dt    = diff --stat=120,130 -M -C
  dls   = diff --name-status -M -C

  #--- Edit files

  # edit eht global config file
  ec = config --global -e
  em    = "!${EDITOR:-vim} $(git ls-files --modified)"
  ea    = "!${EDITOR:-vim} $(git status --short --porcelain|cut -f2 -d' ')"

  # Grep
  g    = "!git --no-pager grep --color --ignore-case -n"

  # GitK
  k     = "!gitk &"

  # Log
  l     = log
  ll    = log --abbrev-commit --pretty=ll                        # compact log
  lt    = log --name-status   --pretty                           # terse log with filenames
  lp    = log --patch -m -c                                      # show merges and compressed diff format
  lpw   = log --patch --word-diff
  l1    = !"git --no-pager ll -10"
  lg    = !git ll --graph
  lu    = log @{upstream}.. --stat --no-merges
  llu    = !git ll @{upstream}..

  # provide one-line git log inculding data and committer name 
  ls = log --pretty=format':%C(yellow)%h %C(blue)%ad%C(red)%d %C(reset)%s%C(green) [%cn]' --decorate --date=short


  # Push
  po    = push origin HEAD

  # Pull
  pu    = pull
  pr    = pull --rebase

  # patch
  # outputs to stdout instead of a-file-file-commit. Handy for dumping commit log into a file
  fps   = format-patch --stdout

  #--- Rebase
  r     = rebase
  ra    = rebase --abort
  rc    = rebase --continue
  ri    = rebase --interactive
  ru    = rebase --onto @{upstream}

  # interactive rebase mostly for squashing/rearranging commits
  # operates on last 2 if not specified otherwise
  rih   = "!f() { git rebase -i HEAD~${1:-'2'}; }; f "

  # rebase up to the very first commit; special case
  riroot   = rebase --interactive --root

  #--- Reset
  # resets staged chages
  rst   = reset

  # resets last commit
  rstl   = reset HEAD^

  # select hunks to unstage|reset
  rstp   = reset --patch

  # reset to @{upstream} with confirmation
  roh   = "! git --no-pager ll -10 @{upstream} && read -p '^C to stop, anykey to continue' Z && git rst --hard @{upstream}"


  #--- Status

  # don't show untracked files
  st    = status --short --branch --untracked-files=no

  # show all
  sta   = status --short --branch --untracked-files=normal

  # sync
  sy     = !git pull --rebase && git push

  sw    = update-index  --skip-worktree 
  nosw  = update-index --no-skip-worktree

  ta    ="!f() { tn=$1; shift; git tag -a $tn -m $tn $@; }; f "

  # Merge Tool
  mt    = "! [ -d /Applications/p4merge.app ] && git mtt p4merge || [ -d /Applications/MacVim.app ] && git mtt mvimdiff || git mtt vimdiff"
  mtt   = mergetool -t

  # Show current `"user" <em@a.il>` configured
  w     = "! echo \"$(git config user.name) <$(git config user.email)> \"" 

  # me2 == "merge to" merge current branch to named one.
  # does 3 operations in one shot unless merge conflicts
  me2   = "!f() { git checkout $1 && git pull && git merge -; }; f "

  # codereview
  # http://gitready.com/advanced/2011/10/21/ribbon-and-catchup-reading-new-commits.html
  ribbon = tag --force _ribbon origin/master
  catchup = log --patch --reverse --topo-order _ribbon..origin/master

  # graphical history showing when commits were added to current branch
  graph = log --graph --pretty=format':%C(yellow)%h%Cblue%d%Creset %s %C(white) %an, %ar%Creset' --abbrev-commit

   update = !git pull --rebase --prune $@ && git submodule update --init --recursive
   wipe = !git add -A && git commit -qm 'WIPE SAVEPOINT' && git reset HEAD~1 --hard



